# Édition Karbowsky

![Représentation du corpus](https://pense.inha.fr/images/Karbowsky-vue-corpus-1.png "")

Projet de mise à disposition des jeux de données produit par l'édition Karbowsky de l'INHA\
Les données se trouvent dans le dossier "[Capta](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/tree/master/Capta)"

# Capta / DecorsKarbowskyPourDoucet

Les manifestes IIIF décrivant les 8 décors de l'hôtel particulier de Jacques Doucet, situé au 19 rue Spontini, dessinés par Adrien Karbowsky.

- [Escalier.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/Escalier.manifest.json)\
Un manifeste IIIF exposant les données de 1 dessin d'Adrien Karbowsky représentant le grand escalier de l'hôtel de Jacques Doucet rue Spontini

- [GrandSalon.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/GrandSalon.manifest.json)\
Un manifeste IIIF exposant les données de 4 dessins et 4 esquisses d'Adrien Karbowsky représentant le grand salon de l'hôtel de Jacques Doucet rue Spontini, de 73 annotations décrivant des œuvres représentées, ainsi que les données de 14 photographies d'époque

- [GrandVestibule.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/GrandVestibule.manifest.json)\
Un manifeste IIIF exposant les données de 4 dessins et 2 esquisses d'Adrien Karbowsky représentant le grand vestibule de l'hôtel de Jacques Doucet rue Spontini, de 15 annotations décrivant des œuvres représentées, ainsi que les données de 3 photographies d'époque

- [PetitBoudoir.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/PetitBoudoir.manifest.json)\
Un manifeste IIIF exposant les données de 1 dessin d'Adrien Karbowsky représentant le petit boudoir de l'hôtel de Jacques Doucet rue Spontini, de 7 annotations décrivant des œuvres représentées, ainsi que les données de 1 photographie d'époque

- [PetitVestibule.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/PetitVestibule.manifest.json)\
Un manifeste IIIF exposant les données de 1 dessin d'Adrien Karbowsky représentant le petit vestibule de l'hôtel de Jacques Doucet rue Spontini, et de 4 annotations décrivant des œuvres représentées

- [SalleAManger.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/SalleAManger.manifest.json)\
Un manifeste IIIF exposant les données de 4 dessins d'Adrien Karbowsky représentant la salle à manger de l'hôtel de Jacques Doucet rue Spontini

- [SalonDesPastels.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/SalonDesPastels.manifest.json)\
Un manifeste IIIF exposant les données de 4 dessins et 3 esquisses d'Adrien Karbowsky représentant le salon des pastels de l'hôtel de Jacques Doucet rue Spontini, de 142 annotations décrivant des œuvres représentées, ainsi que les données de 1 photographie d'époque

- [Vestibule.manifest.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/DecorsKarbowskyPourDoucet/Vestibule.manifest.json)\
Un manifeste IIIF exposant les données de 1 dessin d'Adrien Karbowsky représentant le vestibule de l'hôtel de Jacques Doucet rue Spontini et de 2 annotations décrivant des œuvres représentées



# Capta / CollectionsDoucetRueSpontini

Les données (json) des 228 œuvres, et des 262 personnes physiques ou morales liées, répertoriées dans la base de données [Catalogue des œuvres des collections de Jacques Doucet](https://agorha.inha.fr/database/43) d'[Agorha](https://agorha.inha.fr/)

- [CollectionsDoucet-ProjetKarbowsky-oeuvres.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/CollectionsDoucetRueSpontini/CollectionsDoucet-ProjetKarbowsky-oeuvres.json)\
Toutes les données en format json des œuvres référencées dans l'édition Karbowsky et documentées dans Agorha

- [CollectionsDoucet-ProjetKarbowsky-personnes.json](https://gitlab.inha.fr/snr/LesPlanchesKarbowsky/-/blob/master/Capta/CollectionsDoucetRueSpontini/CollectionsDoucet-ProjetKarbowsky-personnes.json)\
Toutes les données en format json des personnes morales ou physiques référencées dans l'édition Karbowsky et documentées dans Agorha
